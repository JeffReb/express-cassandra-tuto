
# Express restFull API with cassandra tuto
from:
https://www.youtube.com/playlist?list=PLillGF-RfqbZ4aTrhgDerxss_BOgs2Q8h

part1:
https://youtu.be/ivstVNUKAW8

part2:
https://youtu.be/tOAmF0qlzWI

part3:
https://youtu.be/wEAzZ95hWCg

part4:
https://youtu.be/fLOR_7upQp0

part5:
https://youtu.be/Rwogpa6H-xs

part6:
https://youtu.be/akZI52ZUWbo

part7:
https://youtu.be/tll2lsVVWAo

cassandra good practices:
https://www.datastax.com/dev/blog/basic-rules-of-cassandra-data-modeling

## install cassandra
1. - first run cassandra in kubernetes online installed via rancher from bitnami image or in vagrant-test-online (installed via helm bitnami image):
```sh
helm install --set volumePermissions.enabled=true --set persistence.storageClass=managed-nfs-storage --namespace cassandra --name cassandra bitnami/cassandra
```


2. - Check the cluster status by running:

 ```sh
 kubectl exec -it --namespace cassandra $(kubectl get pods --namespace cassandra -l app=cassandra,release=cassandra -o jsonpath='{.items[0].metadata.name}') nodetool status
 ```

   
## connect to cassandra:

### Get your password run:


```sh
export CASSANDRA_PASSWORD=$(kubectl get secret --namespace cassandra cassandra -o jsonpath="{.data.cassandra-password}" | base64 --decode)  <= in cassandra vagrant
			
export CASSANDRA_PASSWORD=$(kubectl get secret --namespace cassandra cassandra-dev -o jsonpath="{.data.cassandra-password}" | base64 --decode)  <= in cassandra k8s online with develop version of cassandra

```
NOTE THE PASSWORD OF CASSANDRA WITH printenv

### Connect to your Cassandra cluster using CQL:

#### Run a Cassandra pod that you can use as a client:

```sh
kubectl run --namespace cassandra cassandra-client --rm --tty -i --restart='Never' --env CASSANDRA_PASSWORD=$CASSANDRA_PASSWORD --image docker.io/bitnami/cassandra:3.11.4-debian-9-r147 -- bash  <= vagrant verison

kubectl run --namespace cassandra cassandra-dev-client --rm --tty -i --restart='Never' --env CASSANDRA_PASSWORD=$CASSANDRA_PASSWORD --image docker.io/bitnami/cassandra:3.11.4-debian-9-r147 -- bash  <= online version

```
#### Connect using the cqlsh client:
```sh
cqlsh -u cassandra -p <password-cassandra> cassandra-dev

```
#### connect to your database from outside the cluster execute the following commands:

```sh
kubectl port-forward --namespace cassandra svc/cassandra 9042:9042  <= vagrant version
kubectl port-forward --namespace cassandra svc/cassandra-dev 9042:9042  <= online version

ssh -L 9042:127.0.0.1:9042 vagrant@vag
ssh -L 9042:127.0.0.1:9042 jeff@online -p 2223

``` 
Launch tableplus to connect to cassandra with setup connection with host:127.0.0.1 port:9042 user:cassandra password: <password-cassandra>


create keyspace via ssh cqlsh (see: https://stph.scenari-community.org/contribs/nos/Cassandra1/co/langage_CQL.html)

```sh
cassandra@cqlsh> DESCRIBE KEYSPACES;
cassandra@cqlsh> CREATE KEYSPACE <keyspace_name> WITH REPLICATION = { 'class' : '<class de la strategie>', 'replication_factor' : <nb réplication> }; // CREATE KEYSPACE test WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
cassandra@cqlsh> USE people;
cassandra@cqlsh:people> CREATE TABLE subscribers (id uuid, email text, first_name text, last_name text, PRIMARY KEY (id, email));
cassandra@cqlsh:people> INSERT INTO subscribers (id, email, first_name, last_name) VALUES (now(), 'user1@gmail.com', 'John', 'Doe');
BEGIN BATCH
... INSERT INTO subscribers (id, email, first_name, last_name) VALUES (now(), 'user2@yahoo.com', 'Bob', 'Johnson')
... INSERT INTO subscribers (id, email, first_name, last_name) VALUES (now(), 'user3@yahoo.com', 'William', 'Smith')
... APPLY BATCH;
```


********** install express project **********
in folder node-express-cassandra-from-traversyMedia/
type: npm install -g express
type: npm install -g express-generator

********* generate the project ************
type: express express-cassandra-tuto

*********** launch your IDE in the folder express-cassandra-tuto ****



******* prepare your project to gitlab-kubernetes *****************
add in your folder:
Dockerfile
gitlab-ci.yaml
.gitignore
README.md
